package me.jonak.upower.traits

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Defines a TileEntity that does someothing with power
  */
trait TraitTilePower
{
	// Abstract fields

	/**
	  * Defines the power currently contained within this tile
	  */
	protected var powerContained: Long

	/**
	  * Defines the maximum power that can be contained within this tile.
	  */
	protected var maxPower: Long


	// Abstract functions

	/**
	  * Getter for [[me.jonak.upower.traits.TraitTilePower.maxPower]]
	  * @return [[me.jonak.upower.traits.TraitTilePower.maxPower]]
	  */
	def getMaxPower: Long = maxPower

	/**
	  * Getter for [[me.jonak.upower.traits.TraitTilePower.powerContained]]
	  * @return [[me.jonak.upower.traits.TraitTilePower.powerContained]]
	  */
	def getPowerContained: Long = powerContained
}
