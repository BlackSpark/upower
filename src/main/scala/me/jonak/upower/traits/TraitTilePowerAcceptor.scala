package me.jonak.upower.traits

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Defines a TileEntity that accepts power
  */
trait TraitTilePowerAcceptor extends TraitTilePower
{
	// Abstract functions

	/**
	  * Adds power to this block
	  *
	  * @param powerReceived The amount of power recieved
	  * @return The amount of power accepted
	  */
	def addPower(powerReceived: Long): Long

}
