package me.jonak.upower.traits

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Defines a TileEntity that outputs power.
  */
trait TraitTilePowerOutputter extends TraitTilePower
{
	// Abstract fields

	/**
	  * Defines the maximum power output of the tile.
	  */
	protected var maxOutput: Long
	/**
	  * Defines the current power output per tick of the tile.
	  */
	protected var currentOutput: Long


	// Concrete functions

	/**
	  * Getter for [[me.jonak.upower.traits.TraitTilePowerOutputter.maxOutput]]
	  * @return [[me.jonak.upower.traits.TraitTilePowerOutputter.maxOutput]]
	  */
	def getMaxOutput: Long = maxOutput

	/**
	  * Getter for [[me.jonak.upower.traits.TraitTilePowerOutputter.currentOutput]]
	  * @return [[me.jonak.upower.traits.TraitTilePowerOutputter.currentOutput]]
	  */
	def getCurrentOutput: Long = currentOutput


	// Abstract functions

	/**
	  * Requests some power from this block
	  *
	  * @param powerRequested The amount of power requested
	  * @return The amount of power taken from the block
	  */
	def usePower(powerRequested: Long): Long
}
