package me.jonak.upower.traits

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Defines a TileEntity that generates power
  */
trait TraitTilePowerGenerator extends TraitTilePowerOutputter
{

}
