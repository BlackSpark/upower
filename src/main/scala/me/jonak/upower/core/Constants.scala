package me.jonak.upower.core

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Mod-wide constants
  */
object Constants
{
	// Static fields

	/** The mod ID **/
	final val MOD_ID = "upower"

	/** The mod's version **/
	final val VERSION = "0.0.0.1"

	/** The mod's language **/
	final val MOD_LANGUAGE = "scala"
}
