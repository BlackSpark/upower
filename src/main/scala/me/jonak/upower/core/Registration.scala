package me.jonak.upower.core

import me.jonak.upower.block.BlockBasicPowerBank
import me.jonak.upower.block.tileentities.TilePowerBank
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.registry.GameRegistry

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Contains static methods to do registration with Forge.
  */
object Registration
{
	// Static functions

	/** Registers Blocks **/
	def doBlockRegistration(): Unit =
	{
		GameRegistry.register(BlockBasicPowerBank)
	}

	/** Registers Items **/
	def doItemRegistration(): Unit =
	{
		GameRegistry.register(BlockBasicPowerBank.getItem)
	}

	/** Registers TileEntities **/
	def doTileEntityRegistration(): Unit =
	{
		GameRegistry.registerTileEntity(classOf[TilePowerBank], "PowerBankTileEntity")
	}

	/** Registers shaped recipes **/
	def doShapedRecipeRegistration(): Unit =
	{
		BlockBasicPowerBank.registerRecipe()
	}

	/** Registers shapeless recipes **/
	def doShapelessRecipeRegistration(): Unit =
	{
		//TODO: Shapeless recipes
	}

	/** Registers Ore Dictionary entries **/
	def doOreDictionaryRegistration(): Unit =
	{
		//TODO: Ore dictionary registrations
	}

	/** Registers item textures **/
	def doItemTextureRegistration(e: FMLInitializationEvent): Unit =
	{
		val renderer = Minecraft.getMinecraft.getRenderItem // get item renderer
		val blocksToRender = Iterator(BlockBasicPowerBank) // Iterator, holds each block to initialize.
		blocksToRender.foreach(item =>
		{
			val rl = Constants.MOD_ID + ":" + item.getName
			println(rl)
			renderer.getItemModelMesher.register(item.getItem, 0, new ModelResourceLocation("upower:powerbank_basic0"))
		})

		//TODO: Make actual items that are separate from blocks.
	}
}
