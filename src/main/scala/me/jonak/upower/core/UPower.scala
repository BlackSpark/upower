package me.jonak.upower.core

import me.jonak.upower.block.BlockBasicPowerBank
import me.jonak.upower.proxy.Proxy
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}
import net.minecraftforge.fml.common.{Mod, SidedProxy}

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Initializes the mod using proxies.
  */
@Mod(modid = Constants.MOD_ID, version = Constants.VERSION, modLanguage = Constants.MOD_LANGUAGE)
object UPower
{

	// Static members

	/** The Universal Power CreativeTab **/
	val uPowerCreativeTab = new CreativeTabs("Universal Power")
	{
		override def getTabIconItem: Item = BlockBasicPowerBank.getItem // Creative tab for Universal Power
	}

	/** Proxy initialization **/
	@SidedProxy(clientSide = "me.jonak.upower.proxy.ClientProxy", serverSide = "me.jonak.upower.proxy.ServerProxy")
	var proxy: Proxy = null

	@EventHandler
	def preInit(event: FMLPreInitializationEvent): Unit =
	{
		proxy.preInit(event)
	}

	@EventHandler
	def init(event: FMLInitializationEvent): Unit =
	{
		proxy.init(event)
	}

	@EventHandler
	def postInit(event: FMLPostInitializationEvent): Unit =
	{
		proxy.postInit(event)
	}
}
