package me.jonak.upower.generics

import me.jonak.upower.block.tileentities.TilePowerBank
import me.jonak.upower.core.UPower
import me.jonak.upower.material.MaterialMachine
import net.minecraft.block.Block
import net.minecraft.block.properties.PropertyInteger
import net.minecraft.block.state.{BlockStateContainer, IBlockState}
import net.minecraft.item.Item
import net.minecraft.util.math.BlockPos
import net.minecraft.world.{IBlockAccess, World}

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Holds the properties of this block.
  */
object BlockGenericPowerBank
{
	// Static fields

	var CHARGE_PERCENT: PropertyInteger = PropertyInteger.create("charge_percent", 0, 10)
}

/**
  * Defines a generic power bank with a maximum energy, output, and a block name.
  *
  * @param maxEnergy The maximum energy this powerbank can hold
  * @param maxOutput The maximum energy per tick this powerbank can output
  * @param name      The unlocalized and registry name for this block
  */
abstract class BlockGenericPowerBank(maxEnergy: Int, maxOutput: Int, name: String = "powerbank_generic") extends Block(new MaterialMachine)
{
	// Static members
	import BlockGenericPowerBank._


	// Abstract members

	/** Defines the item representation of this block **/
	protected val item: Item

	/** Registers the recipe with Forge **/
	def registerRecipe(): Unit


	// Concrete fields

	/** Contains the block's name **/
	private final val blockName: String = name

	/**
	  * Contains the TileEntity associated with this Block.
	  * Assigned in [[me.jonak.upower.generics.BlockGenericPowerBank.createTileEntity]]
	  */
	private var thisTileEntity: TilePowerBank = null


	// Concrete functions

	/** @return The block's name **/
	def getName: String = blockName

	/** @return The item representation of this block **/
	def getItem: Item = item


	// Overridden, concrete functions

	override def hasTileEntity(state: IBlockState): Boolean = true

	override def createTileEntity(worldIn: World, state: IBlockState): TilePowerBank =
	{
		println("Placed Tile: " + (if (worldIn.isRemote) "CLIENT" else "SERVER"))
		thisTileEntity = new TilePowerBank(0, maxEnergy, maxOutput)
		return thisTileEntity
	}

	override def createBlockState: BlockStateContainer = new BlockStateContainer(this, CHARGE_PERCENT)

	override def getActualState(state: IBlockState, worldIn: IBlockAccess, pos: BlockPos): IBlockState =
	{
		val currentPower = thisTileEntity.getPowerContained
		val maxPower = thisTileEntity.getMaxPower
		val percent = TilePowerBank.getPercentTen(maxPower, currentPower)
		return getBlockState.getBaseState.withProperty(CHARGE_PERCENT, percent)
	}

	/** @return The light value based on the charge value of the block **/
	override def getLightValue(state: IBlockState): Int = state.getValue(CHARGE_PERCENT).toInt

	override def getStateFromMeta(meta: Int): IBlockState = getDefaultState

	override def getMetaFromState(state: IBlockState): Int = 0


	// Default constructor

	setUnlocalizedName(name) // Sets the unlocalized name based on initialization parameters.
	setRegistryName(name) // Sets the registry name based on the initialization parameters.
	setCreativeTab(UPower.uPowerCreativeTab) // Sets the tab to the mod's creative tab.
	setDefaultState(getBlockState.getBaseState.withProperty(CHARGE_PERCENT, Integer.valueOf(0))) // Sets the default state to 0% charge.

}
