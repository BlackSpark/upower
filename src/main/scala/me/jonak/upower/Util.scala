package me.jonak.upower

import me.jonak.upower.core.Constants
import net.minecraft.block.Block
import net.minecraft.item.{Item, ItemBlock}

/**
  * @author JonaK
  * @since 5/12/2016
  */

/**
  * This object contains general utility static functions
  */
object Util
{
	// Static functions

	/**
	  * Creates an Item of an Block
	  *
	  * @param block  The block to create an Item of
	  * @param name The name to set as the unlocalized name and registry name
	  * @return The Item.
	  */
	def createItemBlock(block: Block, name: String): Item = new ItemBlock(block).setUnlocalizedName(name).setRegistryName(Constants.MOD_ID, name)
}
