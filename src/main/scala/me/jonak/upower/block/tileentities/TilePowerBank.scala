package me.jonak.upower.block.tileentities

import me.jonak.upower.traits.{TraitTilePowerAcceptor, TraitTilePowerOutputter}
import net.minecraft.block.state.IBlockState
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.NetworkManager
import net.minecraft.network.play.server.SPacketUpdateTileEntity
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

/**
  * @author JonaK
  * @since 5/11/2016
  */


/**
  * Utility class for TilePowerBank.
  */
object TilePowerBank
{
	/** Maps the percent charge to the range (0, 10) **/
	def getPercentTen(maxPower: Long, currentPower: Long): Integer = Integer.valueOf(Math.floor(currentPower.asInstanceOf[Double] / maxPower.asInstanceOf[Double] * 10).asInstanceOf[Int])
}

class TilePowerBank(initialPower: Long = 0, maxPowerInit: Long = 0, maxOutputInit: Long = 0) extends TileEntity with TraitTilePowerAcceptor with TraitTilePowerOutputter
{
	// Static members
	import TilePowerBank._


	// Implemented fields

	override protected var powerContained = initialPower
	override protected var currentOutput = maxOutputInit
	override protected var maxOutput = maxOutputInit
	override protected var maxPower = maxPowerInit
	//TODO: I/O sides


	// Implemented functions

	override def usePower(powerRequested: Long): Long =
	{
		var powerGiven = powerRequested // Defines the amount of power that actually leaves the bank
		if (powerRequested > maxOutput)
			powerGiven = maxOutput
		if (powerContained - powerRequested < 0)
			powerGiven = powerContained

		powerContained -= powerGiven

		return powerGiven
	}

	override def addPower(powerReceived: Long): Long =
	{
		var powerTaken = powerReceived // Defines the amount of power that the bank actually takes.
		val currentPower = powerContained

		if (powerContained + powerReceived > getMaxPower)
		{
			powerTaken = getMaxPower - powerContained // powerReceived - ((getPowerContained + powerReceived) - getMaxPower)
		}

		powerContained += powerTaken

		markDirty()

		val currentState = worldObj.getBlockState(getPos)
		val currentPercentTen = getPercentTen(maxPower, currentPower)
		val newPercentTen = getPercentTen(maxPower, powerContained)

		if (currentPercentTen.compareTo(newPercentTen) != 0)
		{
			worldObj.notifyBlockUpdate( //TODO: Figure out why I need these and make it more concise.
				getPos,
				currentState,
				currentState,
				16
			)
			worldObj.setBlockState(
				getPos,
				currentState,
				16
			)
		}

		println("NEW POWER = " + powerContained)

		return powerTaken // TODO: Check if power added exceeds maxPower and deal with that.
	}

	//TODO: Functions invoked by GUI elements for raising/lowering output, etc
	//TODO: Functions for I/O sides


	// Overridden functions

	override def readFromNBT(tag: NBTTagCompound): Unit =
	{
		// Reads data from NBT to the variables.
		super.readFromNBT(tag)
		maxPower = tag.getLong("maxPower")
		maxOutput = tag.getLong("maxOutput")
		powerContained = tag.getLong("currentPower")
		currentOutput = tag.getLong("currentOutput")
	}

	override def writeToNBT(tag: NBTTagCompound): Unit =
	{
		// Reads data from the variables to NBT
		tag.setLong("maxPower", maxPower)
		tag.setLong("maxOutput", maxOutput)
		tag.setDouble("currentPower", powerContained)
		tag.setLong("currentOutput", currentOutput)
		super.writeToNBT(tag)
	}

	override def shouldRefresh(world: World, pos: BlockPos, oldState: IBlockState, newState: IBlockState): Boolean =
	{
		// Checks if the tile should be recreated
		oldState.getBlock != newState.getBlock
	}

	override def getDescriptionPacket: SPacketUpdateTileEntity =
	{
		// Sends an update packet to the client
		val tag: NBTTagCompound = new NBTTagCompound
		writeToNBT(tag)
		println("SERVER -- Data sent")
		new SPacketUpdateTileEntity(pos, 1, tag)
	}

	override def onDataPacket(net: NetworkManager, packet: SPacketUpdateTileEntity): Unit =
	{
		println("CLIENT -- Data recieved")
		readFromNBT(packet.getNbtCompound)
	}


	// Constructors

	/** No-args constructor for TileEntity re-instantiation at world load **/
	def this()
	{
		this(0, 0, 0)
	}

}