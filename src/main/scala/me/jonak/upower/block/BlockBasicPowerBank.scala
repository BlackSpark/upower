package me.jonak.upower.block

import me.jonak.upower.Util
import me.jonak.upower.generics.BlockGenericPowerBank
import net.minecraft.init.{Blocks, Items}
import net.minecraft.item.{Item, ItemStack}
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.oredict.{OreDictionary, ShapedOreRecipe}

/**
  * @author JonaK
  * @since 5/12/2016
  */

/**
  * Defines a basic power bank, extending [[me.jonak.upower.generics.BlockGenericPowerBank]].
  *
  * Max Power: 100
  * Max Output: 5
  * Name: "powerbank_basic"
  */
object BlockBasicPowerBank extends BlockGenericPowerBank(100, 5, "powerbank_basic") // Holds 100 power units, with a max output of 5 units power/unit time, with a name "powerbank_basic"
{

	// Implemented members

	override protected val item: Item = Util.createItemBlock(this, "powerbank_basic")

	override def registerRecipe(): Unit =
	{
		val redstoneStack = new ItemStack(Items.redstone)
		val redstoneBlockStack = new ItemStack(Blocks.redstone_block)
		val woodStack = new ItemStack(Blocks.planks, 1, OreDictionary.WILDCARD_VALUE)
		GameRegistry.addRecipe(
			new ShapedOreRecipe(
				new ItemStack(BlockBasicPowerBank.getItem),
				"rrr",
				"RwR",
				"rrr",
				Character.valueOf('r'), redstoneStack,
				Character.valueOf('R'), redstoneBlockStack,
				Character.valueOf('w'), "plankWood"
			)
		)
	}
}
