package me.jonak.upower.material

import net.minecraft.block.material.{MapColor, Material}

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Defines the MachineMaterial used by all machine blocks in this mod.
  */
class MaterialMachine extends Material(MapColor.grayColor)
{
	// Default constructor

	//TODO: Define this material more.
	setRequiresTool()
}
