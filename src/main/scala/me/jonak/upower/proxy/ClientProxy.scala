package me.jonak.upower.proxy

import me.jonak.upower.core.Registration
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Typical client proxy
  */
class ClientProxy extends Proxy
{
	// Concrete functions

	@EventHandler
	override def preInit(event: FMLPreInitializationEvent): Unit =
	{
		super.preInit(event)
	}

	/** Registration.doItemTextureRegistration(event) is only called client-side **/
	@EventHandler
	override def init(event: FMLInitializationEvent): Unit =
	{
		super.init(event)
		Registration.doItemTextureRegistration(event)
	}

	@EventHandler
	override def postInit(event: FMLPostInitializationEvent): Unit =
	{
		super.postInit(event)
	}
}
