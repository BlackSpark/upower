package me.jonak.upower.proxy

import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}

/**
  * @author JonaK
  * @since 5/10/2016
  */

/**
  * Typical server proxy
  */
class ServerProxy extends Proxy
{
	// Concrete functions

	@EventHandler
	override def preInit(event: FMLPreInitializationEvent): Unit =
	{
		super.preInit(event)
	}

	@EventHandler
	override def init(event: FMLInitializationEvent): Unit =
	{
		super.init(event)
	}

	@EventHandler
	override def postInit(event: FMLPostInitializationEvent): Unit =
	{
		super.postInit(event)
	}
}