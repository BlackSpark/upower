package me.jonak.upower.proxy

import me.jonak.upower.core.Registration
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}

/**
  * @author JonaK
  * @since 5/15/2016
  */

trait Proxy
{
	// Concrete functions

	@EventHandler
	def preInit(event: FMLPreInitializationEvent): Unit =
	{ // Does pre-init registration (blocks, items, and tile entities)
		Registration.doBlockRegistration()
		Registration.doItemRegistration()
		Registration.doTileEntityRegistration()
		Registration.doOreDictionaryRegistration()
		println("UPower Registered!")
	}

	@EventHandler
	def init(event: FMLInitializationEvent): Unit =
	{ // Does recipe registration, ore dictionary registration, and texture registration.
		println("UPower's loading!")
		Registration.doShapedRecipeRegistration()
		Registration.doShapelessRecipeRegistration()
	}

	@EventHandler
	def postInit(event: FMLPostInitializationEvent): Unit =
	{ //TODO: Something. Also maybe implementing a nicer log system than println.
		println("uPower Done loading!")
	}
}
